// 모듈 추출
var fs = require('fs');
var connect = require('connect');

// 변수 설정 (연습이니께 변수로 서버 붙으면 쿼리로)
var id = 'sangyeon';
var password = '12345';

// 서버생성
var server = connect.createServer();

//미들웨어 사용
server.use(connect.cookieParser());
server.use(connect.bodyParser());
server.use(connect.router(function(app){
    // GET - /Login
    app.get('/Login',function(req,res){
      if(req.cookies.auth == 'true'){
        //응답
        res.writeHead(200,{'Content-Type':'text/html'});
        res.end('<h1>Login Success</h1>');
      }else{
        // 로그인 안됬을 경우
        fs.readFile('login.html',function(error,data){
          // 응답
          res.writeHead(200,{'Content-Type':'text/html'});
          res.end(data);
        })
      }
    })

    // POST - /Login
    app.post('/Login',function(req,res){
      if(req.body.id == id && req.body.password == password){
        // 로그인 성공
        res.writeHead(302,{
          'Location':'/Login',
          'Set-Cookie':['auth = true']
        });
        res.end();
      }else{
        // 로그인 실패
        res.writeHead(200,{'Content-Type':'text/html'});
        res.end('<h1>Login FAIL</h1>');
      }
    })

}));

// 서버실행
server.listen(52273,function(){
  console.log('Server running at http://127.0.0.1:52273');
})
