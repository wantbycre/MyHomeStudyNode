// 모듈 추출
var http = require('http');
var fs = require('fs');
var ejs = require('ejs');

// 서버를 생성 실행
http.createServer(function(req,res){
  // EJS파일 일기
  fs.readFile('EJSPage.ejs','utf-8',function(error,data){
      res.writeHead(200,{'Content-Type':'text/html'});
      res.end(ejs.render(data,{
        name:'RintianTta',
        description:'Hello EJS With Node.js .. !'
      }));
  });
}).listen(52273,function(){
  console.log('Server Running at http://127.0.0.1:52273');
})
