var connect = require('connect');

connect.createServer(connect.cookieParser(),connect.router(function(app){
    // GET - /Setcookie
    app.get('/Setcookie',function(req,res) {
      // 응답
      res.writeHead(200,{
        'Content-Type':'text/html',
        'Set-Cookie':['breakfast = toast','dinner = lunch']
      });
      res.end('<a href="/GetCookie">Go To Get Cookie</a>');
    })

    // GET - /GetCookie
    app.get('/GetCookie',function(req,res){
      // 쿠키 출력
      var output = JSON.stringify(req.cookies);

      // 응답
      res.writeHead(200,{'Content-Type':'text/html'});
      res.end('<h1>'+output+'</h1>');
    })
})).listen(52273,function(){
  console.log('Server running at http://127.0.0.1:52273');
})
