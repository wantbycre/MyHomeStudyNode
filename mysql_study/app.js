// 모듈 추출
var fs = require('fs');
var connect = require('connect');
var mysql = require('mysql');
var ejs = require('ejs');

// DB연결
var client = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'apmsetup',
    database : 'Company'
});

// 서버생성
connect.createServer(connect.bodyParser(),connect.router(function(app){
    // GET - /Listen
    app.get('/',function(req,res){
      // List파일 읽기
      fs.readFile('List.html','utf-8',function(error,data){
        // DB 쿼리 실행
        client.query('SELECT * FROM products',function(error,result){
          res.writeHead(200,{'Content-Type':'text/html'});
          res.end(ejs.render(data,{
            data:result
          }))
        });
      });
    });

    // GET - /DELETE/:id
    app.get('/Delete/:id',function(req,res){
      // 삭제 쿼리 실행
      client.query('DELETE FROM products WHERE id=?',[req.params.id]);

      // 응답
      res.writeHead(302,{'Location':'/'});
      res.end();
    });

    // GET - /INSERT
    app.get('/Insert',function(req,res){
      // Insert.html 파일 읽기
      fs.readFile('Insert.html','utf-8',function(error,data){
        // 응답
        res.writeHead(200,{'Content-Type':'text/html'});
        res.end(data);
      })
    });

    // POST - /INSERT
    app.post('/Insert',function(req,res){
      // 변수 선언
      var body = req.body;

      // 데이터베이스 쿼리를 실행합니다.
      client.query('INSERT INTO products (name,modelnumber,series) VALUES(?,?,?)',[body.name, body.modelnumber, body.series]);

      // 응답
      res.writeHead(302,{'Location':'/'});
      res.end();
    });

    // GET - /EDIT/:id
    app.get('/Edit/:id',function(req,res){
       //Edit.html 파일 읽기
       fs.readFile('Edit.html','utf-8',function(error,data){
         // 데이터 베이스 쿼리 실행
         client.query('SELECT * FROM products WHERE id = ?',[req.params.id],
         function(error,result){
           // 응답합니다.
           res.writeHead(200,{'Content-Type':'text/html'});
           res.end(ejs.render(data,{
             data:result[0]
           }));
         });
       });
    });

    // POST - /EDIT/:id
    app.post('/Edit/:id',function(req,res){
      // 변수 선언
      var body = req.body;

      // 데이터베이스 쿼리를 실행
      client.query('UPDATE products SET name=?, modelnumber=?, series=? WHERE id=?',
      [body.name,body.modelnumber,body.series,req.params.id]);

      // 응답
      res.writeHead(302,{'Location':'/'});
      res.end();
    });

})).listen(52273,function(){
  console.log('connect Server Ok~~');
})
