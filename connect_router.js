var connect = require('connect');

connect.createServer(connect.router(function(app){
    // get - /home/index
    app.get('/home/index',function(req,res,next){
      // 응답
      res.writeHead(200,{'Content-Type':'text/html'});
      res.write('<h1>Index Page</h1>');
      res.end();
    });

    // /home/about
    app.get('/home/about' , function(req,res,next){
      res.writeHead(200,{'Content-Type':'text/html'});
      res.write('<h1>About Page</h1>');
      res.end();
    });
})).listen(52273,function(){
  console.log('Server running at http://127.0.0.1:52273');
})
